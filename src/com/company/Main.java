package com.company;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        int b;
        Scanner scanner = new Scanner(System.in);
        b = scanner.nextInt();
        System.out.println("是奇數還是偶數? " +
                ((b&1) != 0 ? "奇數" : "偶數"));
    }
}
